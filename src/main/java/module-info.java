module co.edu.uniquindio.prog2.tricky {
    requires javafx.controls;
    requires javafx.fxml;


    opens co.edu.uniquindio.prog2.tricky to javafx.fxml;
    exports co.edu.uniquindio.prog2.tricky;
}
package co.edu.uniquindio.prog2.tricky;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class HelloController {
    @FXML
    private Label playerLabel;

    @FXML
    private GridPane gridPane;

    @FXML
    public void initialize() {
        System.out.println("second");
    }

    private int[][] matrix = new int[3][3];
    private final String PLAYER_X = "X";
    private final String PLAYER_O = "O";
    private final String EMPTY = " ";
    private boolean stopActions = false;

    private String currentPlayer = "X";

    public void clear() {
        for (Node node: gridPane.getChildren()) {
            if (node.getClass().equals(Button.class)) {
                ((Button) node).setText(" ");
            }
        }
        matrix = new int[3][3];
        currentPlayer = PLAYER_X;
    }

    @FXML
    protected void onButtonClick(ActionEvent event) {
        if (stopActions) {
            return;
        }
        Button button = (Button) event.getSource();
        Integer column = GridPane.getColumnIndex(button);
        Integer row = GridPane.getRowIndex(button);

        if (button.getText().equals(EMPTY)) {
            if (currentPlayer.equals(PLAYER_X)) {
                button.setText(PLAYER_X);
                this.validate(PLAYER_X, row, column);
                currentPlayer = PLAYER_O;
            }
            else {
                button.setText(PLAYER_O);
                this.validate(PLAYER_O, row, column);
                currentPlayer = PLAYER_X;
            }

        }
    }

    private void validate(String player, Integer row, Integer column) {
        int playerInt = 1;
        if (player.equals(PLAYER_O)) {
            playerInt = -1;
        }
        matrix[row.intValue()][column.intValue()] = playerInt;

        int sumaVertical = 0;
        int sumaHorizontal = 0;
        int sumaDiagonal0 = 0;
        int sumaDiagonal1 = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (i == j) {
                    sumaDiagonal0 += matrix[i][j];
                    sumaDiagonal1 += matrix[i][matrix.length - 1 - i];
                }
                sumaHorizontal += matrix[i][j];
                sumaVertical += matrix[j][i];
            }
            if (sumaVertical == 3 || sumaHorizontal == 3 || sumaDiagonal0 == 3 || sumaDiagonal1 == 3) {
                System.out.println("El ganador es X");
                stopActions = true;
            }
            else if (sumaVertical == -3 || sumaHorizontal == -3 || sumaDiagonal0 == -3 || sumaDiagonal1 == -3) {
                System.out.println("El ganador es O");
                stopActions = true;
            }
            sumaVertical = 0;
            sumaHorizontal = 0;
        }
    }
}